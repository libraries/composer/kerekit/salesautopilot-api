# API client for SalesAutopilot email marketing

Features:
- Provides basic `delete()`, `get()`, `post()` and `put()` methods for any
  resource.
- Handles authentication from constructor arguments.
- Formats requests and parses responses, throws generic Error instance if fails.

## Installation

Install the latest version with

```bash
$ composer require kerekit/salesautopilot-api
```

## Basic Usage

```php
<?php

use Kerekit\SalesAutopilotApi\Client;

// Init API client with credentials
$api = new Client ('my-username', 'my-api-key');

// Get subscribers by list ID
$list = $api->get ('list/1234');

// Output first subscriber's (if any) email address
$subscriber = reset ($list);
if ($subscriber === false) {
    echo "No subscriber on the list.\n";
} else {
    echo "First subscriber on the list has the email: '$subscriber[email]'\n";
}
```

## API documentation
See the <a href="https://support.salesautopilot.com/hc/en-us/articles/360012079093-SalesAutopilot-API" target="_blank">official API documentation</a> for available resources, parameters, request and response examples.
