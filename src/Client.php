<?php

namespace Kerekit\SalesAutopilotApi;

class Client
{
    public const VALID_RESPONSE_TYPES = ['array', 'integer'];

    protected string $pass;
    protected string $user;

    public function __construct (string $user, string $pass)
    {
        $this->pass = $pass;
        $this->user = $user;
    }

    public function delete (string $resource): void
    {
        $response = $this->_call ('DELETE', $resource);
        if ($response !== 1) {
            throw new \Error ("Expected the response of '1', got '$response' instead.");
        }
    }

    /** @return array|int */
    public function get (string $resource, array $params = [])
    {
        return $this->_call ('GET', $resource, $params);
    }

    /** @return int Either the ID of created item, or 1 as a sign of success */
    public function post (string $resource, array $data = []): int
    {
        $response = $this->_call ('POST', $resource, $data);
        if ($response < 1) {
            throw new \Error ("Got a response of '$resopnse', that can't be any good...");
        }
        return $response;
    }

    /** @return int Either the number of updated items, or 1 as a sign of success */
    public function put (string $resource, array $data = []): int
    {
        return $this->_call ('PUT', $resource, $data);
    }

    /** @return array|int */
    protected function _call (
        string $method,
        string $resource,
        array $data = []
    )
    {
        // Init request
        $url = "https://restapi.emesz.com/$resource";
        $request = curl_init ($url);
        curl_setopt_array ($request, [
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HEADER         => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERPWD        => "$this->user:$this->pass",
        ]);

        // Add data (to URL or payload)
        if (count ($data)) {
            switch ($method) {
                case 'GET':
                    $query = http_build_query ($data);
                    curl_setopt ($request, CURLOPT_URL, "$url?$query");
                    break;

                case 'POST':
                case 'PUT':
                    $payload = json_encode ($data);
                    curl_setopt ($request, CURLOPT_POSTFIELDS, $payload);
                    break;

                default:
                    throw new \Error ("Sending data with method '$method' is unexpected.");
                    break;
            }
        }

        // Send request
        $responseStr = curl_exec ($request);
        $errno       = curl_errno ($request);
        $error       = curl_error ($request);
        if ($errno !== CURLE_OK) {
            throw new \Error ("Request failed with HTTP error $errno ($error). Response body: '$responseStr'.");
        }
        curl_close ($request);

        // Parse response
        $response = json_decode ($responseStr, true);
        $errno    = json_last_error ();
        $error    = json_last_error_msg ();
        if ($errno !== JSON_ERROR_NONE) {
            throw new \Error ("Failed to parse response as JSON ($errno): '$error'. Raw response: '$responseStr'.");
        }

        // Check response type
        $type = gettype ($response);
        if (!in_array ($type, self::VALID_RESPONSE_TYPES)) {
            throw new \Error ("Response has the type '$type', while only the following ones are regarded successful: " . implode (', ', self::VALID_RESPONSE_TYPES) . '.');
        }

        return $response;
    }
}
